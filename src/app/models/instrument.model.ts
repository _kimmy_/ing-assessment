// dit model beschrijft het instrument welke binnen komen 

export interface IInstrumentResponse {
  instruments: IInstrument[];
}

export interface IInstrument {
  name: string;
  currentPrice: IPrice;
  priceDetails: IPriceDetails;
}

export interface IPrice {
  value: number;
  unit: string;
  percent: number;
}

export interface IPriceDetails {
  closePrice: IPrice;
  highPrice: IPrice;
  lowPrice: IPrice;
  openPrice: IPrice;
}
export class Instrument {
    name: string;
    price: number;
    highPrice: number;
    lowPrice: number;
    closePrice: number;

  public static parse(data: IInstrument): Instrument {
      const instrument = new Instrument();
      instrument.name = data.name;
      instrument.price = data.currentPrice.value;
      instrument.closePrice = data.priceDetails.closePrice.value;
      instrument.highPrice = data.priceDetails.highPrice.value;
      instrument.lowPrice = data.priceDetails.lowPrice.value;
      instrument.closePrice = data.priceDetails.openPrice.value;

      return instrument;
  }
}
