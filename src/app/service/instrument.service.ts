import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Instrument, IInstrument, IInstrumentResponse} from '../models/instrument.model';
import { HttpClient } from '@angular/common/http';
import { Observable, timer } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InstrumentService {

  constructor(
    private http: HttpClient
  ) { }

  public pollInstruments(): Observable<Instrument[]> {
    return timer(0, 2000)
      .pipe(
        switchMap(() => this.getInstruments())
      );
  }

  public getInstruments(): Observable<Instrument[]> {
    const url: string = environment.api;
    // const url: string = 'https://services.ing.nl/api/securities/mobile/markets/stockmarkets/AEX';
    // console.dir(url + Instrument);
    return this.http.get<IInstrumentResponse>(url).pipe(
      map((response: IInstrumentResponse) => response.instruments),
      map((instruments: IInstrument[]) => instruments.map((data: IInstrument) => Instrument.parse(data))),
   );
  }
}