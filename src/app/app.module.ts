import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DetailComponent } from './page/detail/detail.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { InstrumnetsComponent } from './page/instruments/instruments.component';
import { InstrumentComponent } from './components/instrument/instrument.component';
import { InstrumentsComponent } from './components/instruments/instruments.component';


@NgModule({
  declarations: [
    AppComponent,
    DetailComponent,
    InstrumnetsComponent,
    HeaderComponent,
    FooterComponent,
    InstrumentComponent,
    InstrumentsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
