import { Component, Input, OnInit } from '@angular/core';
import { Instrument } from 'src/app/models/instrument.model';

// dit is een dom component heeft geen logica geeft alleen data door


@Component({
  selector: 'app-instrument',
  templateUrl: './instrument.component.html',
  styleUrls: ['./instrument.component.css']
})
export class InstrumentComponent {

  @Input() instrument: Instrument;
  
}
