import { Component} from '@angular/core';
import { InstrumentService } from 'src/app/service/instrument.service';
import { Observable } from 'rxjs';
import { Instrument } from '../../models/instrument.model';

@Component({
  selector: 'app-instruments',
  templateUrl: './instruments.component.html',
  styleUrls: ['./instruments.component.css']
})
export class InstrumentsComponent  {

  public instruments$: Observable<Instrument[]>;

  public selectedInstrument: Instrument;

  constructor( private instrumentService: InstrumentService
  ) {
    this.instruments$ = this.instrumentService.pollInstruments();
  }

  public selectInstrument(instrument: Instrument): void {
    this.selectedInstrument = instrument;
  }

  public deselectInstrument(): void {
    this.selectedInstrument = null;
  }

}